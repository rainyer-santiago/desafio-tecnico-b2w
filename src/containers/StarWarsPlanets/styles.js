import styled from 'styled-components'
import { SW_YELLOW } from '../../constants/style-constants'
export const Container = styled.div`
  display: flex;
  height: 100%;
  padding: 50px;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export const NextButton = styled.button`
  width: 200px;
  font-size: 18px;
  height: 50px;
  color: black;
  background: ${SW_YELLOW} 0% 0% no-repeat padding-box;
  box-shadow: 0px 8px 15px ${SW_YELLOW};
  border-radius: 5px;
  border-color: ${SW_YELLOW};
  opacity: 1;
  outline: none;
`
