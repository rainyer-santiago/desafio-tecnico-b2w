import React, { useEffect, useState } from 'react'
import AxiosInstance from '../../services/requestService'
import { PlanetCard, Loading } from '../../components'
import { Container, NextButton } from './styles'

export default function StartWarsPlanets() {
  const [planets, setPlanets] = useState([])
  const [loading, setLoading] = useState(false)
  const [currentPlanet, setCurrentPlanet] = useState(null)

  async function fetchAllPlanets() {
    let planets = []
    const response = await AxiosInstance.get('planets/?format=json')
    let responseData = response.data
    planets = planets.concat(responseData.results)

    while (responseData.next) {
      const nextResponse = await AxiosInstance.get(responseData.next)
      planets = planets.concat(nextResponse.data.results)
      responseData = nextResponse.data
    }

    return planets.filter(planet => planet.name !== 'unknown')
  }

  function showRandomPlanet() {
    var currentPlanet = planets[Math.floor(Math.random() * planets.length)]
    setCurrentPlanet(currentPlanet)
  }

  useEffect(() => {
    const loadData = async () => {
      try {
        setLoading(true)
        const planets = await fetchAllPlanets()
        setPlanets(planets)
        setCurrentPlanet(planets[0])
      } catch (error) {
        alert('It was not possible to fecth planets')
      } finally {
        setLoading(false)
      }
    }
    loadData()
  }, [])

  return (
    <Container>
      {currentPlanet && <PlanetCard planet={currentPlanet} />}
      {loading && (
        <Loading fadeIn={'none'} loadingMessage={'Loading all planets...'} />
      )}
      {!loading && <NextButton onClick={showRandomPlanet}>Next</NextButton>}
    </Container>
  )
}
