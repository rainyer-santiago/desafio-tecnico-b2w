import axios from 'axios'
import { BASE_URL } from '../constants/api-constants'

export default axios.create({
  baseURL: BASE_URL,
})
