import React from 'react'
import Spinner from 'react-spinkit'
import styled from 'styled-components'
import { SW_YELLOW } from '../../constants/style-constants'

const LoadingContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const LoadingMessage = styled.span`
color:${props => (props.color ? props.color : SW_YELLOW)}
  display: block;
  margin: 20px;
  text-align: center;
  font-weight: bold;
`

export default function Loading({ loadingMessage, messageColor }) {
  return (
    <LoadingContainer>
      <Spinner color={SW_YELLOW} name="folding-cube" />
      {loadingMessage && (
        <LoadingMessage color={messageColor}>{loadingMessage}</LoadingMessage>
      )}
    </LoadingContainer>
  )
}
