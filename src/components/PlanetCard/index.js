import React from 'react'
import {
  Card,
  CardTitle,
  PlanetInfo,
  CardContent,
  PlanetInfoLabel,
} from './styles'

export default function PlanetCard({ planet }) {
  return (
    <Card>
      <CardTitle>{planet.name}</CardTitle>
      <CardContent>
        <PlanetInfo leftAlign>
          <PlanetInfoLabel>Population: </PlanetInfoLabel>
          {planet.population}
        </PlanetInfo>
        <PlanetInfo leftAlign>
          <PlanetInfoLabel>Climate: </PlanetInfoLabel>
          {planet.climate}
        </PlanetInfo>
        <PlanetInfo leftAlign>
          <PlanetInfoLabel>Terrain: </PlanetInfoLabel>
          {planet.terrain}
        </PlanetInfo>
        <PlanetInfo bold>Featured in {planet.films.length} films</PlanetInfo>
      </CardContent>
    </Card>
  )
}
