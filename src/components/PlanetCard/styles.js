import styled from 'styled-components'

export const Card = styled.div`
  background-color: white;
  justify-content: center;
  align-items: center;
  text-align: center;
  border-radius: 10px;
  margin-bottom: 30px;
  box-shadow: 1px 1px 3px 3px #3333333d;
`

export const CardTitle = styled.h1`
  text-align: center;
  color: #000;
`

export const CardContent = styled.div`
  display: flex;
  min-width: 300px;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  padding: 30px;
  flex: 1;
  border-top: 1px solid #ddd;
`

export const PlanetInfo = styled.h4`
  align-self: ${props => (props.leftAlign ? 'flex-start' : 'auto')};
  text-align: ${props => (props.leftAlign ? 'left' : 'center')};
  color: #333333;
  font-weight: ${props => (props.bold ? 'bold' : 'normal')};
`

export const PlanetInfoLabel = styled.span`
  font-weight: bold;
`
